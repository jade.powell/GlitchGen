

# GlitchGen
## Introduction
GlitchGen is a python package that allows users to generate 19 different types of fake glitches using an already trained machine learning GAN model. The GAN is trained on GravitySpy glitches. The package is based on the paper "Generating Transient Noise Artefacts in Gravitational-Wave Detector Data with Generative Adversarial Networks" by Powell et. al. https://arxiv.org/abs/2207.00207. It can produce glitch images or time series injections in frame files. 

## Table of contents
* [Installation](#Installation)
* [Documentation](#Documentation)
* [Contributors](#Contributors)

## Installation

From Source:
```console
$ git clone https://git.ligo.org/jade.powell/GlitchGen
$ cd GlitchGen
```

To install this package from source:
```console
$ python setup.py install
```

## Documentation

GlitchGan can be used to produce time series or images of glitches 

#### A list of glitch names that can be generated with this package. 
1080Lines, 1400Ripples, Air_Compressor, Blip, Chirp, Helix, Koi_Fish, Light_Modulation,
Low_Frequency_Burst, Low_Frequency_Lines, Paired_Doves, Power_Line, Repeating_Blips,
Scattered_Light, Scratchy, Tomte, Violin_Mode, Wandering_Line, Whistle

#### Formats
Glitches can be saved as png images, or time series in a gwf or hdf5 format.

#### Signal to Noise Ratio (SNR)
The user can specify one SNR for all glitches, request a realistic distribution of SNRs, 
or input their own SNR distribution. Number of SNR values in array must be equal to the 
number of requested glitches. 

#### Timeseries frames
The name= option can be used to specify the channel name. t0 can be used to specify GPS start time.
Otherwise defaults to t0 = 1238166018.0. The sample rate used is 4096 Hz. noise=True will add
design sensitivity Gaussian LIGO noise to the frames as well as the glitches. Length= for the total 
length of the data. 

Use position= to specify the injection times of the glitches. The position value should be (the time
after the start of the frame - 1) x 4096. 

## Examples

First, we need to import the Generator module and instatiate a Generator object.

```python
from GlitchGen import Generator
generator = Generator.Generator()
```

Generate and save images of 50 different Koi Fish glitches.
```python
generator.generate("Koi_Fish", 50)
generator.save_as_png("path/to/file", clear_queue=True)
```

Generate and save time series of 2 of each glitch type, each with an SNR of 12, and save in a frame file.
```python
generator.generate_all(2, SNR=12)
generator.save_as_timeseries("path/to/file","name",format="gwf", clear_queue=True)
```

Generate and save 96 time series Blip glitches, with a realistic SNR distribution, and save in a frame file.
```python
generator.generate("Blip", 96, SNR='realistic')
generator.save_as_timeseries("path/to/file", "name", format="hdf5", clear_queue=True)
```

Generate 5 seconds of Gaussian LIGO data with a Koi Fish at 2s, 3s, and 4s after the start of the frame. 
```python
generator.generate("Koi_Fish", 3, SNR=100)
generator.save_as_timeseries("/Users/jpowell/projects/GlitchGen/test",name="L1:STRAIN",format="gwf", noise=True, 
    clear_queue=True, length=5, position=([4096, 8192, 12288]))
```

## Contributors
Seamus Staite, Jade Powell, Ling Sun, Katinka Gereb, Paul Lasky & Markus Dollmann

