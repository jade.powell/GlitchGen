import setuptools

with open('README.md', 'r', encoding='utf-8') as fh:
    long_description = fh.read()

setuptools.setup(
    name='GlitchGen',
    author=['Seamus Staite', 'Jade Powell', 'Lilli Sun', 'Paul Lasky', 'Katinka Gereb'],
    author_email=['seamus.staite@gmail.com', 'jade.powell@ligo.org', 'ling.sun@anu.edu.au', 'paul.lasky@monash.edu', 'katinka.gereb@eliiza.com.au'],
    description='A package that generates glitches in gravitational-wave detector data.',
    keywords='gravitational-wave, glitches, GANs',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://git.ligo.org/jade.powell/GlitchGen',
    project_urls={
        'Documentation': '',
        'Bug Reports':
        'https://git.ligo.org/jade.powell/GlitchGen/issues',
        'Source Code': 'https://git.ligo.org/jade.powell/GlitchGen',
    },
    include_package_data=True,
    package_dir={'': 'src'},
    packages=setuptools.find_packages(where='src'),
    classifiers=[
        'Development Status :: 3 - Alpha',
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GPL License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8.5',
)
